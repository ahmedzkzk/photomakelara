<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SlideController extends Controller
{
    public function index()
    {
    	$active = 'slides';

    	return view('admin.slides.index',compact('active'));
    }

    public function create()
    {
    	$active = 'add_slide';
    	return view('admin.slides.index',compact('active'));
    	
    }
}
