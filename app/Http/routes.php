<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard/', 'DashboardController@index');

Route::get('/dashboard/settings', function () {
		
	$active = 'settings';
	
    return view('admin.index',compact('active'));
});

// Route::get('/dashboard/slides', 'SlideController@index');

Route::resource('/dashboard/slides','SlideController');


