@extends('admin.app')

@section('title','عارض الصور')

@section('content')

	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">عارض الصور</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading text-left">
                           <a href="{{ url('/dashboard/slides/create') }}" class="btn btn-success">إضافة عارض</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الصوره</th>
                                            <th>العمليات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd gradeX">
                                            <td>Trident</td>
                                            <td>Internet Explorer 4.0</td>
                                            <td>
                                            	<button type="button" data-toggle="tooltip" data-placement="top" title="معاينه" class="btn btn-warning btn-circle"><i class="fa fa-pencil-square-o"></i>
					                            </button>

					                            <button type="button" data-toggle="tooltip" data-placement="top" title="حذف" class="btn btn-danger btn-circle"><i class="fa fa-times"></i>
					                            </button>
                                            </td>
                                        </tr>
                                        <tr class="even gradeC">
                                            <td>Trident</td>
                                            <td>Internet Explorer 5.0</td>
                                            
                                            <td>
                                            	<button type="button" data-toggle="tooltip" data-placement="top" title="معاينه" class="btn btn-warning btn-circle"><i class="fa fa-pencil-square-o"></i>
					                            </button>

					                            <button type="button" data-toggle="tooltip" data-placement="top" title="حذف" class="btn btn-danger btn-circle"><i class="fa fa-times"></i>
					                            </button>
                                            </td>
                                        </tr>
                                        <tr class="odd gradeA">
                                            <td>Trident</td>
                                            <td>Internet Explorer 5.5</td>
                                            
                                            <td>
                                            	<button type="button" data-toggle="tooltip" data-placement="top" title="معاينه" class="btn btn-warning btn-circle"><i class="fa fa-pencil-square-o"></i>
					                            </button>

					                            <button type="button" data-toggle="tooltip" data-placement="top" title="حذف" class="btn btn-danger btn-circle"><i class="fa fa-times"></i>
					                            </button>
                                            </td>
                                        </tr>
                                        <tr class="even gradeA">
                                            <td>Trident</td>
                                            <td>Internet Explorer 6</td>
                                            
                                            <td>
                                            	<button type="button" data-toggle="tooltip" data-placement="top" title="معاينه" class="btn btn-warning btn-circle"><i class="fa fa-pencil-square-o"></i>
					                            </button>

					                            <button type="button" data-toggle="tooltip" data-placement="top" title="حذف" class="btn btn-danger btn-circle"><i class="fa fa-times"></i>
					                            </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

@stop