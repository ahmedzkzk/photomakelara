<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a class ="{{ $active == 'dashboard' ? 'active' : '' }}" href="{{ url('/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> لوحة التحكم</a>
            </li>
            <li>
                <a class ="{{ $active == 'settings' ? 'active' : '' }}" href="{{ url('/dashboard/settings') }}"><i class="fa fa-cog fa-fw"></i> إعدادات الموقع</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-film fa-fw"></i> عارض الصور<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class="{{ $active == 'slides' ? 'active' : '' }}" href="{{url('/dashboard/slides')}}">عرض الكل</a>
                    </li>
                    <li>
                        <a class="{{ $active == 'add_slide' ? 'active' : '' }}" href="morris.html">إضافة</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
           
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->